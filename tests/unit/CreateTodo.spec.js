import Vue from "vue";
import CreateTodo from "@/components/CreateTodo";
import {expect} from 'chai';

describe('CreateTodo.vue', () => {
    let Constructor;
    let AddComponent;
    beforeEach(()=>{
        Constructor = Vue.extend(CreateTodo);
        AddComponent = new Constructor().$mount();
    })

    it('should be init',()=>{
        expect(AddComponent.$el.firstChild.tagName).to.equals('INPUT')
        expect(AddComponent.$el.lastChild.tagName).to.equals('BUTTON')
    })

    it('add todo',()=>{
        AddComponent.title = "test todo";
        const button = AddComponent.$el.querySelector('button');
        const clickEvent = new window.Event('click');
        button.dispatchEvent(clickEvent);
        AddComponent._watcher.run();
    })
})