import Vue from "vue";
import Todos from "@/components/Todos";
import {expect} from 'chai';


describe('Todos.vue', () => {
    it('list todo', () => {
        const Constructor = Vue.extend(Todos);
        const ListComponent = new Constructor().$mount();

        ListComponent.todoList.push({Id:123, Title:"test", Completed:false});
        ListComponent.todoList.push({Id:124, Title:"test1", Completed:false});
        ListComponent.todoList.push({Id:125, Title:"test2", Completed:false});
        ListComponent.todoList.push({Id:126, Title:"test3", Completed:false});

        expect(ListComponent.todoList.length).to.equals(4);
        expect(ListComponent.todoList[0].Title).to.contain('test');
    })
})